# Current status

We've never received the ordered Edgeswitches and this tool was not finished.

# EdgeSwitch to Cumulus Linux (nclu) command converter

This shell is supposed to be used as a login shell on a Cumulus Linux VX appliance for testing the
[EdgeSwitch VLAN module](https://docs.ansible.com/ansible/latest/collections/community/network/edgeswitch_vlan_module.html#ansible-collections-community-network-edgeswitch-vlan-module)
by converting edgeswitch commands to nclu commands without having a EdgeSwitch virtual
appliance handy (they seem to not exist as of today).

The EdgeSwitch VLAN module only supports a very limited feature set and so does the command
converter. It interactively reads EdgeSwitch commands from stdin and outputs
[NCLU](https://docs.nvidia.com/networking-ethernet-software/cumulus-linux-44/System-Configuration/Network-Command-Line-Utility-NCLU/) commands.

# Commands

VLAN creation

```
vlan database
vlan 100
vlan name 100 "clients"
vlan 1337
vlan name 1337 "admins"
exit
```

Interface configuration

```
interface 0/1
vlan pvid 100
vlan tagging 1337
exit
```

# Usage

Run tests with `make`. If the environment variable `DEBUG` exists, it will output nclu
commands instead of executing them.
