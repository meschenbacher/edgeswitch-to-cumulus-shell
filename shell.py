#!/usr/bin/env python3

import sys, os
import socket
import re
import subprocess


class Section():
    """
    a section usually is 'vlan' or 'int' but could also contain dashes e.g. 'addr-family-v4'
    """
    def __init__(self, args):
        self.args = args
    def __str__(self):
        return '-'.join(self.args)


class EdgeSwitch:

    def __del__(self):
        if self.inhibit_descruction:
            return
        self.commit()

    def commit(self):
        self.exec('net commit')

    @property
    def section(self):
        if len(self.sections) > 0:
            return '(%s)' % ('-'.join([ str(s) for s in self.sections ]),)
        else:
            return ''

    def __init__(self):
        self.hostname = socket.gethostname()
        self.sections = list()
        self.inhibit_descruction = False

    def print(self, msg):
        print(msg)

    def exec(self, *msg):
        if os.environ.get('DEBUG', None):
            self.print(' '.join([ str(w) for w in msg ]))
        else:
            args = ' '.join([ str(w) for w in msg ]).split(' ')
            subprocess.check_call(args)

    def prompt(self):
        try:
            if not sys.stdin.isatty():
                line = input()
            else:
                line = input(self.hostname+self.section+"#")
        except EOFError:
            sys.exit(0)
        return line.rstrip()

    def reload(self, args):
        self.inhibit_descruction = True
        self.print("Going down")
        sys.exit(0)

    def exit(self):
        if len(self.sections) > 0:
            return self.sections.pop()
        else:
            sys.exit(0)

    def vlan_database(self, args):
        if len(args) != 0:
            self.print('invalid command vlan xxx', args)
            return
        if ['vlan'] in [ s.args for s in self.sections ]:
            self.print('unable to enter section vlan')
            return
        self.sections.append(Section(['vlan']))

    def vlan_id(self, args):
        if len(args) != 1:
            self.print('unsupported command vlan', args)
            return
        if len(self.sections) != 1 or self.sections[0].args[0] != 'vlan':
            self.print('command only allowed in vlan database')
            return
        self.exec('net add vlan', int(args[0]))
        self.exec('net add bridge bridge vids', int(args[0]))

    def no_vlan_id(self, args):
        if len(args) != 1:
            self.print('unsupported command vlan', args)
            return
        if len(self.sections) != 1 or self.sections[0].args[0] != 'vlan':
            self.print('command only allowed in vlan database')
            return
        self.exec('net del vlan', int(args[0]))
        # vlan will be removed from bridge automatically
        # TODO also from other interfaces?

    def vlan_name(self, args):
        if not len(args) >= 2:
            print('unsupported command vlan name', args)
            return
        if len(self.sections) != 1 or self.sections[0].args[0] != 'vlan':
            print('command only allowed in vlan database')
            return
        description = (' '.join(args[1:])).strip('"')
        self.exec('net add vlan', int(args[0]), 'alias %s' % (description,))

    def interface(self, args):
        # strip all previous sections when entering a new interface
        while len(self.sections) > 0:
            self.sections.pop()

        self.sections.append(Section(['interface', args[0]]))

    def interface_pvid(self, args):
        port = self.sections[-1].args[1]
        interface = int(re.match('\d/(\d+)', port).group(1))
        pvid = int(args[0])
        self.exec('net add interface swp%s bridge access %d' % (interface, pvid))

    def interface_tagging(self, args):
        port = self.sections[-1].args[1]
        interface = int(re.match('\d/(\d+)', port).group(1))
        tagged_vlan = int(args[0])
        self.exec('net add interface swp%s bridge vids %d' % (interface, tagged_vlan))

    def section_position(self, name, position):
        return len(switch.sections) > position and switch.sections[position].args == name

    def section_position_startswith(self, name, position):
        return len(switch.sections) > position and switch.sections[position].args[0:len(name)] == name


switch = EdgeSwitch()

def main():
    while True:
        line = switch.prompt()
        args = line.split(" ")
        if line == '':
            continue
        elif line == 'vlan database':
            switch.vlan_database(args[2:])
        elif line == 'reload':
            switch.reload(args[:1])
        elif line == 'exit':
            switch.exit()
        elif switch.section_position(['vlan'], 0) and args[0] == 'vlan' and len(args) == 2 and re.match('\d+', args[1]):
            switch.vlan_id(args[1:])
        elif switch.section_position(['vlan'], 0) and len(args) == 3 and (args[0], args[1]) == ('no', 'vlan') and re.match('\d+', args[2]):
            switch.no_vlan_id(args[2:])
        elif switch.section_position(['vlan'], 0) and args[0] == 'vlan' and len(args) >= 4 and args[1] == 'name' and re.match('\d+', args[2]):
            switch.vlan_name(args[2:])
        elif (len(switch.sections) == 0 or switch.section_position_startswith(['interface'], 0)) and len(args) == 2 and args[0] == 'interface':
            switch.interface(args[1:])
        elif switch.section_position_startswith(['interface'], 0) and len(args) == 3 and (args[0], args[1]) == ('vlan', 'pvid'):
            switch.interface_pvid(args[2:])
        elif switch.section_position_startswith(['interface'], 0) and len(args) == 3 and (args[0], args[1]) == ('vlan', 'tagging'):
            switch.interface_tagging(args[2:])
        elif args[0] in ('wr', 'wri', 'writ', 'write'):
            switch.commit()
        else:
            print("command `%s' not valid in section `%s'" % (line, switch.section))

if __name__ == '__main__':
    main()
