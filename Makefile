.PHONY: test

all: test

test:
	export DEBUG=1; bash -c 'diff -u tests/vlan_base.result <(cat tests/vlan_base | python3 shell.py)'
	export DEBUG=1; bash -c 'diff -u tests/interfaces_base.result <(cat tests/interfaces_base | python3 shell.py)'
	export DEBUG=1; bash -c 'diff -u tests/commit.result <(cat tests/commit | python3 shell.py)'
